#include "spkctrl.h"
#include "wsmountpoint.h"

#include <Core/App/skeventloop.h>
#include <Core/System/Filesystem/skfsutils.h>
#include <Core/System/Filesystem/skfileinfoslist.h>
#include <Core/Containers/skdatabuffer.h>
#include <Core/Containers/skringbuffer.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define LABIALS_DIR         "labials.d/"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SpkCtrl, AbstractController)
{
    publisher = nullptr;
    speaker = nullptr;

    SlotSet(onSpeechStarted);
    SlotSet(onSpeechFinished);
    /*SignalSet(speechStarted);
    SignalSet(speechFinished);*/
    SlotSet(sendPhoneme);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SpkCtrl::setup(SpkConfig &config)
{
    cfg = config;
}

void SpkCtrl::onInit()
{
    speaker = new SkTextToSpeech(this);
    speaker->setObjectName(this, "Speaker");

    AssertKiller(!speaker->enable());

    speaker->setVoice(cfg.language.c_str(), cfg.gender, cfg.age);
    speaker->setWordsForMinutes(cfg.wordsForMinutes);
    speaker->setVolume(cfg.volume);
    speaker->setPitch(cfg.pitch);
    speaker->setRange(cfg.range);
    speaker->setCapitals(cfg.capitals);
    speaker->setWordsPauses(cfg.wordsPauses);
    //speaker->setPunctuations(espeakPUNCT_NONE);

    params.set(true, 1, speaker->sampleRate(), 1024, AFMT_SHORT);

    publisher = new SkFlowAudioPublisher(this);
    publisher->setObjectName(this, "Publisher");
    publisher->setClient(async);
    publisher->setup(params);

    inputBuffer.setObjectName(this, "PCM");
    publisher->setInputBuffer(&inputBuffer);
    publisher->enableFftGaussianFilter(true);
    publisher->start();

    Attach(speaker, speechStarted, this, onSpeechStarted, SkDirect);
    Attach(speaker, speechFinished, this, onSpeechFinished, SkDirect);
}

void SpkCtrl::onStart()
{}

void SpkCtrl::onStop()
{
    while(!currentValidLabials.isEmpty())
        delete currentValidLabials.dequeue();

    if (conn)
        conn->sendCommand("RESET_CURRENT_LABIAL");

    ObjectWarning("STOPPED");
}

void SpkCtrl::onClose()
{
    publisher->stop();
    speaker->disable();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SpkCtrl, sendPhoneme)
{
    SilentSlotArgsWarning();

    if (!conn)
        return;

    SkVariantList phonemes;
    SkFileInfosList *l = new SkFileInfosList(this);
    SkFsUtils::ls(LABIALS_DIR, l, false);

    l->open();

    while(l->next())
        if (!l->currentPathIsDir() && l->currentPath().endsWith(".json"))
        {
            SkArgsMap m;
            SkFsUtils::readJSON(l->currentPath().c_str(), m);
            phonemes << m;
        }

    l->close();
    l->destroyLater();

    if (!phonemes.isEmpty())
    {
        SkVariant val(phonemes);
        conn->sendCommand("SET_KNOWN_PHONEMES", &val);
    }

    conn->sendCommand("RESET_CURRENT_LABIAL");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SpkCtrl::onSetConnection()
{
    eventLoop()->invokeSlot(sendPhoneme_SLOT, this, this);
}

void SpkCtrl::onUnSetConnection()
{}


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SpkCtrl::onFastTick()
{
    if (!isRunning())
        return;

    if (!inputBuffer.isEmpty())
    {
        if (tickTimeChrono.stop() > params.getTickTimePeriod())
        {
            tickTimeChrono.start();
            publisher->tick();
        }
    }

    if (!currentValidLabials.isEmpty() && labialsChrono.stop() > currentValidLabials.first()->startMS)
        sendCurrentLabial();
}

void SpkCtrl::onSlowTick()
{}

void SpkCtrl::onOneSecondTick()
{}


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(SpkCtrl, onSpeechStarted)
{
    SilentSlotArgsWarning();

    //speechStarted();
}

SlotImpl(SpkCtrl, onSpeechFinished)
{
    SilentSlotArgsWarning();

    //speechFinished();
    checkLabials();
    checkSpeakerPcm();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SpkCtrl::speech(CStr *txt)
{
    speaker->synth(txt);
}

void SpkCtrl::makeVoiceTest()
{
    SkString str = "Questo è solo un test di funzionamento " \
                   "per lo speaker. La prova di testo permette di verificare " \
                   "la sillabazione, oltre alla gestione dei fonemi per il movimento " \
                   "labiale del mio modello tridimensionale ed i parametri di default per la voce.";

    /*SkString str = "Salve, mi presento!" \
                     " ... Mi chiamo Zero-Inox e sono costituito da un'applicazione mediamente complessa per SK-robot," \
                     " che si avvale di altri satelliti per offrire un servizio di assistente virtuale." \
                     " ... purtroppo, per ora, la mia voce sintetica non è molto espressiva e risulta abbastanza innaturale. " \
                     " Sono in grado di: ascoltare, vedere e parlare; posso anche azionare procedure e comandi sul mio sistema operativo o" \
                     " su quello di altri satelliti remoti collaboranti sulla rete di flusso." \
                     " Mi avvalgo dell'elaborazione offerta dai moduli per il rilevamento di: movimento, facce e" \
                     " codici grafici di vario genere come i codici QR, o a barre;" \
                     " utilizzo anche altri satelliti esterni per ottenere il servizio di riconoscimento vocale basato su inferenza locale! ..." \
                     " Rappresento un modello di applicazione che potrebbe evolvere in un vero prodotto utile, uscendo dal ruolo di demo." \
                     " In futuro, alcune migliorie potrebbero trasformarmi in un fedele assistente operativo!";*/

    speaker->synth(str.c_str());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SpkCtrl::setPhonemeLabial(SkArgsMap &phonemeLabial)
{
    SkString phoneme = phonemeLabial["phoneme"].toString();

    SkString labialFilePath(LABIALS_DIR);
    labialFilePath.append("ph-");
    labialFilePath.append(phoneme);
    labialFilePath.append(".json");

    phonemeLabial["valid"] = true;

    SkFsUtils::writeJSON(labialFilePath.c_str(), phonemeLabial, true);
    ObjectWarning("WROTE new labial config for phoneme: " << phoneme << " [" << labialFilePath << "]");
}

void SpkCtrl::onPhonemeLabialRequest(CStr *phonemeName)
{
    SkString labialFilePath(LABIALS_DIR);
    labialFilePath.append("ph-");
    labialFilePath.append(phonemeName);
    labialFilePath.append(".json");

    if (SkFsUtils::exists(labialFilePath.c_str()))
    {
        SkVariant labialParams;
        SkFsUtils::readJSON(labialFilePath.c_str(), labialParams);
        //cout << labialParams << "\n";
        conn->sendCommand("SET_CURRENT_LABIAL", &labialParams);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SpkCtrl::checkLabials()
{
    SkQueue<SkTextToSpeechPhoneme *> &phonemesQueue = speaker->phonemes();
    ObjectWarning("CHECKING phonemes production: " << phonemesQueue.count());

    while(!phonemesQueue.isEmpty())
    {
        SkTextToSpeechPhoneme *ph = phonemesQueue.dequeue();

        if (ph->phoneme.startsWith("("))
        {
            PhonemeLabialValues *labial = new PhonemeLabialValues;
            labial->isInterruption = true;
            labial->startMS = ph->timePos;
            labial->durationMS = ph->duration;

            currentValidLabials.enqueue(labial);

            delete ph;
            continue;
        }

        SkString labialFilePath(LABIALS_DIR);
        labialFilePath.append("ph-");
        labialFilePath.append(ph->phoneme);
        labialFilePath.append(".json");

        if (SkFsUtils::exists(labialFilePath.c_str()))
        {
            SkArgsMap m;
            SkFsUtils::readJSON(labialFilePath.c_str(), m);

            if (m["valid"].toBool())
            {
                PhonemeLabialValues *labial = new PhonemeLabialValues;
                labial->isInterruption = false;
                labial->startMS = ph->timePos;
                labial->durationMS = ph->duration;
                labial->paramsVal = m;

                currentValidLabials.enqueue(labial);
            }
        }

        else
        {
            SkArgsMap m;
            m["phoneme"] = ph->phoneme;
            m["valid"] = false;

            SkFsUtils::writeJSON(labialFilePath.c_str(), m, true);
            ObjectWarning("WROTE new labial config for phoneme: " << ph->phoneme);
        }

        delete ph;
    }
}

void SpkCtrl::checkSpeakerPcm()
{
    /*if (currentValidLabials.isEmpty())
        return;*/

    SkRingBuffer &pcm = speaker->pcmData();

    SkDataBuffer b;
    b.setData(&pcm);

    if (b.isEmpty())
        return;

    if (conn)
    {
        if (!currentValidLabials.isEmpty())
        {
            sendCurrentLabial();
            labialsChrono.start();
        }

        conn->sendData(b);
        ObjectMessage("Speech PCM buffer SENT: " << b.size() << " B");
    }

    //

    ULong cSz = inputBuffer.getCanonicalSize();
    ULong zeros = b.size() % cSz;

    inputBuffer.addData(b.data(), b.size());

    if (zeros)
    {
        b.clear();
        b.fill(0, (cSz-zeros));
        inputBuffer.addData(b.data(), b.size());
        ObjectWarning("Inserted silence-bytes to reach canonical-size [" << cSz << "]: " << b.size() << " B");
    }

    onFastTick();
}

void SpkCtrl::sendCurrentLabial()
{
    if (!conn)
        return;

    if (currentValidLabials.isEmpty())
        return;

    PhonemeLabialValues *labial = currentValidLabials.dequeue();

    if (labial->isInterruption)
        conn->sendCommand("RESET_CURRENT_LABIAL");

    else
        conn->sendCommand("SET_CURRENT_LABIAL", &labial->paramsVal);

    delete labial;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void SpkCtrl::onFlowDataCome(SkFlowChannelData &)
{}

bool SpkCtrl::isOwnedChannel(SkFlowChanID)
{
    return false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
