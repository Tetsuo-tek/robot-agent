#ifndef AVATARCTRL_H
#define AVATARCTRL_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/Object/skabstractworkerobject.h>

#include "spkctrl.h"
#include "asrctrl.h"
#include "cvctrl.h"
#include "langctrl.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct AvatarConfig
{
    SpkConfig spkCfg;
    AsrConfig asrCfg;
    CvConfig cvCfg;
    LangConfig langCfg;
};


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class WsConnection;

class AvatarCtrl extends SkAbstractWorkerObject
{
    AvatarConfig cfg;

    bool running;
    WsConnection *conn;
    SkFlowAsync *async;

    bool frontendReady;
    bool audioEnabled;
    bool micEnabled;
    bool camEnabled;

    SpkCtrl *spkCtrl;
    AsrCtrl *asrCtrl;
    CvCtrl *cvCtrl;
    LangCtrl * langCtrl;

    bool speeching;
    SkQueue<SkString> speechBlocks;

    ULong randomBlinkInterval;
    SkElapsedTimeMillis eyesBlinkChrono;

    public:
        Constructor(AvatarCtrl, SkAbstractWorkerObject);

        void init(AvatarConfig &config, SkFlowAsync *flow);
        void close();

        void setConnection(WsConnection *connection);

        void onChannelAdded(SkFlowChannel *ch);
        void onChannelRemoved(SkFlowChannel *ch);

        void onCamDataCome(CVoid *data, ULong sz);
        void onMicDataCome(CVoid *data, ULong sz);
        void onFlowDataCome(SkFlowChannelData &dataContainer);
        void onWsConnEvtCmdCome(SkArgsMap &evt);

        Slot(onFastTick);
        Slot(onSlowTick);
        Slot(onOneSecondTick);

        bool isRunning();

    private:
        void start();
        void stop();

        void analyze(SkArgsMap &transcription);

        void updateCommand(SkWorkerCommand *)       override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // AVATARCTRL_H
