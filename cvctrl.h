#ifndef CVCTRL_H
#define CVCTRL_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Time/skelapsedtime.h>
#include <Multimedia/Image/skmat.h>
#include <Multimedia/Image/skimagedecoder.h>
#include <Core/System/Network/FlowNetwork/skflowvideomjpegpublisher.h>
#include <Core/System/Network/FlowNetwork/skflowgenericsubscriber.h>
#include <Multimedia/Image/Detection/skmotiondetect.h>

#if defined(ENABLE_CUDA)
    #include "opencv2/cudaobjdetect.hpp"
    #include "opencv2/cudaimgproc.hpp"
    #include "opencv2/cudawarping.hpp"

    using namespace cv::cuda;
#endif

#include "abstractcontroller.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct CvConfig
{
    SkString motionDetectorName;
    SkString faceDetectorName;
    SkString codeDetectorName;
};

class CvCtrl extends AbstractController
{
    CvConfig cfg;

    SkMat f;
    SkMat y;

#if defined(ENABLE_CUDA)
    GpuMat frame_GPU;
#endif

    SkFlowVideoMJpegPublisher *publisher;
    SkImageDecoder camDecoder;

    SkFlowGenericSubscriber *motionDetectorCanvas;
    SkFlowGenericSubscriber *motionDetectorBlocks;
    SkFlowGenericSubscriber *faceDetectorBoxes;
    SkFlowGenericSubscriber *codeDetectorMeshes;

    SkString motionDetectorInputChanName;
    SkString faceDetectorInputChanName;
    SkString codeDetectorInputChanName;

    Point watchingTo;
    double ambiguousWatchingInterval;
    SkElapsedTime ambiguousWatchingChrono;

    SkVariant currentMotionCanvasVal;
    SkVariant currentMotionBlocksVal;
    SkVariant currentFaceBoxesVal;
    SkVariant currentCodeMeshesVal;

    bool facesBoxesSent;
    bool motionCanvasSent;
    bool motionBlocksSent;
    bool codeMeshesSent;

    SkElapsedTime resetDetectionChrono;

    public:
        Constructor(CvCtrl, AbstractController);

        void setup(CvConfig &config);

        void addJPegData(CVoid *data, ULong sz);

        Slot(onPublisherReady);
        Slot(onSubscriberReady);
        Slot(onSubscriberDataCome);

        void onFlowDataCome(SkFlowChannelData &dataContainer)   override;
        bool isOwnedChannel(SkFlowChanID chanID)                override;

    private:
#if defined(ENABLE_CUDA)
        bool checkCudaSupport();
        void vect2DtoVarianList(Vec2i vect, SkVariantList &l);
        void vect3DtoVarianList(Vec3i vect, SkVariantList &l);
#endif

        void onInit()                                           override;
        void onStart()                                          override;
        void onStop()                                           override;
        void onClose()                                          override;
        void onSetConnection()                                  override;
        void onUnSetConnection()                                override;
        void onFastTick()                                       override;
        void onSlowTick()                                       override;
        void onOneSecondTick()                                  override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // CVCTRL_H
