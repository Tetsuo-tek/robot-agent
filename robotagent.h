#ifndef ROBOTAGENT_H
#define ROBOTAGENT_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/App/skapp.h>

#include "wsmountpoint.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class RobotAgent extends SkFlowAsync
{
    SkCli *cli;
    SkString agentName;
    AvatarCtrl *avtCtrl;
    WsMountpoint *agentMp;
    SkElapsedTime checkChrono;

    public:
        Constructor(RobotAgent, SkFlowAsync);

        Slot(init);
        Slot(inputData);
        Slot(oneSecTick);
        Slot(quit);

        Slot(onDisconnection);
        Slot(exitFromKernelSignal);
        
    private:
        void buildASync();
        void buildAvatarCtrl();
        void buildWsServer();

        void onFlowDataCome();

        void onChannelAdded(SkFlowChanID chanID)                override;
        void onChannelRemoved(SkFlowChanID chanID)              override;

};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // ROBOTAGENT_H
