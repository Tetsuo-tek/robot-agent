﻿#include "robotagent.h"

#include <Core/System/skosenv.h>
#include "Core/Containers/skringbuffer.h"

ConstructorImpl(RobotAgent, SkFlowAsync)
{
    cli = skApp->appCli();

    avtCtrl = nullptr;
    agentMp = nullptr;

    agentName = "Agent";
    setObjectName("RobotAgent");

    SlotSet(init);
    SlotSet(inputData);
    SlotSet(oneSecTick);
    SlotSet(onDisconnection);
    SlotSet(exitFromKernelSignal);
    SlotSet(quit);

    Attach(skApp->started_SIG, pulse, this, init, SkOneShotQueued);

    Attach(this, channelDataAvailable, this, inputData, SkDirect);
    Attach(this, disconnected, this, onDisconnection, SkQueued);

    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAgent, init)
{
    SilentSlotArgsWarning();

    agentName = cli->value("--agent-name").toString();

    buildASync();
    buildAvatarCtrl();
    buildWsServer();
}

void RobotAgent::buildASync()
{
    AssertKiller(!osEnv()->existsEnvironmentVar("ROBOT_ADDRESS"));
    SkString robotAddress = osEnv()->getEnvironmentVar("ROBOT_ADDRESS");

    if (robotAddress.startsWith("local:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("local:")];
        ObjectWarning("Selected Robot LOCAL-address [$ROBOT_ADDRESS]: " << robotAddress);

        AssertKiller(!localConnect(robotAddress.c_str()));
    }

    else if (robotAddress.startsWith("tcp:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("tcp:")];
        ObjectWarning("Selected Robot TCP-address [$ROBOT_ADDRESS]: " << robotAddress);

        SkStringList robotAddressParsed;
        robotAddress.split(":", robotAddressParsed);
        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);

        if (robotAddressParsed.count() == 1)
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));

        else
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));
    }

    SkString userName = "Agent";
    SkString userPasswd = "password";

    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));

    SkFlowSync *sync = new SkFlowSync(this);

    if (isTcpSocket())
        AssertKiller(!sync->tcpConnect(getTcpAddress(), getTcpPort()));

    else
        AssertKiller(!sync->localConnect(getUnixPath()));

    AssertKiller(!sync->login(userName.c_str(), userPasswd.c_str()));

    if (!sync->existsOptionalPairDb(userName.c_str()))
        sync->addDatabase(userName.c_str());

    sync->close();
    sync->destroyLater();

    setCurrentDbName(userName.c_str());

    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);
}

void RobotAgent::buildAvatarCtrl()
{
    AssertKiller(!cli->isUsed("--asr-daemons"));

    AvatarConfig avtCfg;

    SkString str = cli->value("--asr-daemons").toString();
    str.split(",", avtCfg.asrCfg.asrDaemonsNames);

    avtCfg.asrCfg.silenceThresholdUpper = cli->value("--silence-threshold-upper").toFloat();
    avtCfg.asrCfg.silenceThresholdLower = cli->value("--silence-threshold-lower").toFloat();
    avtCfg.asrCfg.maxSilenceInterval = cli->value("--max-continuous-silence").toFloat();
    avtCfg.asrCfg.maxRecordingDuration = cli->value("--max-recording-clip").toFloat();
    avtCfg.asrCfg.bufferingingInterval = cli->value("--mic-buffering-interval").toFloat();

    avtCfg.spkCfg.language = cli->value("--speaker-language").toString();
    avtCfg.spkCfg.gender = cli->value("--speaker-gender").toUInt8();
    avtCfg.spkCfg.age = cli->value("--speaker-age").toUInt8();
    avtCfg.spkCfg.wordsForMinutes = cli->value("--speaker-words-for-minutes").toInt();
    avtCfg.spkCfg.wordsPauses = cli->value("--speaker-words-pause").toInt();
    avtCfg.spkCfg.volume = cli->value("--speaker-volume").toInt();
    avtCfg.spkCfg.pitch = cli->value("--speaker-pitch").toInt();
    avtCfg.spkCfg.range = cli->value("--speaker-pitch-range").toInt();
    avtCfg.spkCfg.capitals = cli->value("--speaker-capitals").toInt();

    avtCfg.cvCfg.motionDetectorName = cli->value("--motion-detector-name").toString();
    avtCfg.cvCfg.faceDetectorName = cli->value("--face-detector-name").toString();
    avtCfg.cvCfg.codeDetectorName = cli->value("--code-detector-name").toString();

    avtCfg.langCfg.nlpName = cli->value("--nlp-name").toString();

    avtCtrl = new AvatarCtrl(this);
    avtCtrl->setObjectName(this, "AvatarCtrl");
    avtCtrl->init(avtCfg, this);
}

void RobotAgent::buildWsServer()
{
    AssertKiller(!cli->isUsed("--ssl-cert") || !cli->isUsed("--ssl-key"));

    WsMountpointConfig svrCfg;

    svrCfg.svrName = agentName;
    svrCfg.svrName.append(".WsServer");

    svrCfg.address = cli->value("--http-listen-address").toString();
    svrCfg.port = cli->value("--http-listen-port").toInt();
    svrCfg.maxQueuedSockets = cli->value("--max-queued-sockets").toInt();
    svrCfg.certificate = cli->value("--ssl-cert").toString();
    svrCfg.key = cli->value("--ssl-key").toString();

    agentMp = new WsMountpoint(this);
    agentMp->setObjectName(this, "WsMp");

    AssertKiller(!agentMp->open(this, svrCfg, avtCtrl));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotAgent::onChannelAdded(SkFlowChanID chanID)
{
    if (!avtCtrl)
        return;

    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);
    avtCtrl->onChannelAdded(ch);
}

void RobotAgent::onChannelRemoved(SkFlowChanID chanID)
{
    if (!avtCtrl)
        return;

    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);
    avtCtrl->onChannelRemoved(ch);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAgent, inputData)
{
    SilentSlotArgsWarning();
    onFlowDataCome();
}

void RobotAgent::onFlowDataCome()
{
    if (nextData())
    {
        if (!agentMp->enabled())
            return;

        SkFlowChannelData &dataContainer = getCurrentData();
        avtCtrl->onFlowDataCome(dataContainer);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAgent, oneSecTick)
{
    SilentSlotArgsWarning();

    if (checkChrono.stop() > 2.)
    {
        if (isConnected())
            checkService();

        checkChrono.start();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAgent, onDisconnection)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting on disconnection..");
    eventLoop()->invokeSlot(quit_SLOT, this, this);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAgent, exitFromKernelSignal)
{
    SilentSlotArgsWarning();

    if (Arg_Int != SIGINT)
        return;

    ObjectMessage("Forcing application exit with CTRL+C");
    skApp->invokeSlot(quit_SLOT, this, this);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAgent, quit)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting ..");

    agentMp->close();
    avtCtrl->close();

    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
