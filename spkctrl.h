#ifndef SPKCTRL_H
#define SPKCTRL_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Time/skelapsedtime.h>
#include <Multimedia/Audio/Voice/sktexttospeech.h>
#include <Core/System/Network/FlowNetwork/skflowaudiopublisher.h>

#include "abstractcontroller.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SpkConfig
{
    SkString language;
    uint8_t gender;
    uint8_t age;
    int wordsForMinutes;
    int volume;
    int pitch;
    int range;
    int capitals;
    int wordsPauses;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct PhonemeLabialValues
{
    bool isInterruption;
    SkString phonemeFile;
    SkVariant paramsVal;
    ULong startMS;
    ULong durationMS;

    PhonemeLabialValues()
    {
        startMS = 0;
        durationMS = 0;
    }
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SpkCtrl extends AbstractController
{
    SpkConfig cfg;

    SkTextToSpeech *speaker;
    SkFlowAudioPublisher *publisher;

    SkAudioParameters params;
    SkRingBuffer inputBuffer;
    SkElapsedTime tickTimeChrono;

    SkQueue<PhonemeLabialValues *> currentValidLabials;
    SkElapsedTimeMillis labialsChrono;

    public:
        Constructor(SpkCtrl, AbstractController);

        void setup(SpkConfig &config);

        void speech(CStr *txt);
        void makeVoiceTest();

        void setPhonemeLabial(SkArgsMap &phonemeLabial);
        void onPhonemeLabialRequest(CStr *phonemeName);

        Slot(onSpeechStarted);
        Slot(onSpeechFinished);

        /*Signal(speechStarted);
        Signal(speechFinished);*/

        Slot(sendPhoneme);

        void onFlowDataCome(SkFlowChannelData &)                override;
        bool isOwnedChannel(SkFlowChanID)                       override;

    private:
        void checkLabials();
        void checkSpeakerPcm();
        void sendCurrentLabial();

        void onInit()                                           override;
        void onStart()                                          override;
        void onStop()                                           override;
        void onClose()                                          override;
        void onSetConnection()                                  override;
        void onUnSetConnection()                                override;
        void onFastTick()                                       override;
        void onSlowTick()                                       override;
        void onOneSecondTick()                                  override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SPKCTRL_H
