#ifndef ASRCCTRL_H
#define ASRCCTRL_H

#include <Core/System/Time/skelapsedtime.h>
#include <Core/System/Network/FlowNetwork/skflowaudiopublisher.h>

#include "abstractcontroller.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum AsrDaemonState
{
    AsrD_OFFLINE,
    AsrD_IDLE,
    AsrD_PROCESSING,
    AsrD_PROCESSED
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct AsrDaemon
{
    SkString name;
    AsrDaemonState st;

    //INPUT
    SkString micChanName;
    SkFlowChannel *micChan;

    //OUTPUT
    SkString transcriptionChanName;
    SkFlowChannel *transcriptionChan;

    //SkString lastTranscription;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum AsrState
{
    Asr_DISABLED,
    Asr_IDLE,
    Asr_RECORDING,
    Asr_STOPPING
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct AsrConfig
{
    SkStringList asrDaemonsNames;

    float silenceThresholdUpper;
    float silenceThresholdLower;
    float maxSilenceInterval;   //max duration of continuous-silence under a record-session; the check can be interrupted if there is a new speaking-activity
    float bufferingingInterval;     //time-interval to stop from checked continuos-silence-interval; cannot be interrupted
    float maxRecordingDuration;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class AsrCtrl extends AbstractController
{
    AsrConfig cfg;

    SkTreeMap<SkString, AsrDaemon *> daemons;
    SkTreeMap<SkFlowChanID, AsrDaemon *> pcmDaemonsChan;
    SkTreeMap<SkFlowChanID, AsrDaemon *> transcriptionDaemonsChan;
    SkQueue<AsrDaemon *> recordingDaemon;
    AsrDaemon *currentRecordingDaemon;

    SkQueue<SkDataBuffer *> inputPcmWindow;
    SkAudioParameters params;
    SkRingBuffer inputBuffer;
    SkFlowAudioPublisher *publisher;
    SkAudioBuffer vuCheckerAudioBuffer;
    float vuCurrentValue;

    SkFlowChanID daemonCtrlChan;

    AsrState st;
    int connectedDaemons;
    int processingDaemons;

    SkElapsedTime silenceCheckChrono;
    bool silenceCheckStarted;

    SkElapsedTime stoppingChrono;
    SkElapsedTime recordingChrono;

    SkArgsMap transcription;

    public:
        Constructor(AsrCtrl, AbstractController);

        void setup(AsrConfig &config);

        void onChannelAdded(SkFlowChannel *ch);
        void onChannelRemoved(SkFlowChannel *ch);

        void addPcmData(CVoid *data, ULong sz);
        SkArgsMap &lastTranscription();

        static CStr *asrStateToString(AsrState st);
        static CStr *asrDaemonStateToString(AsrDaemonState st);

        void onFlowDataCome(SkFlowChannelData &dataContainer)   override;
        bool isOwnedChannel(SkFlowChanID chanID)                override;

    private:
        void tick();

        AsrDaemon *selectNextAsrDaemon();

        void startCurrentRecording();
        void stopCurrentRecording();

        bool checkForChangeToIDLE();
        bool checkForChangeToBUSY();

        void changedState();

        void onInit()                                           override;
        void onStart()                                          override;
        void onStop()                                           override;
        void onClose()                                          override;
        void onSetConnection()                                  override;
        void onUnSetConnection()                                override;
        void onFastTick()                                       override;
        void onSlowTick()                                       override;
        void onOneSecondTick()                                  override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // ASRCCTRL_H
