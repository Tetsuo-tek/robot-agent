#include "langctrl.h"

ConstructorImpl(LangCtrl, AbstractController)
{
    nlpGeneration = nullptr;

    promptServiceID = -1;
    processing = false;

    SlotSet(onNlpReady);
    SlotSet(onNlpDataCome);
    SignalSet(onNlpBlockReady);
    SlotSet(onServiceResponseReceived);
}

void LangCtrl::setup(LangConfig &config)
{
    cfg = config;

    promptServiceName = cfg.nlpName;
    promptServiceName.append(".LLM.Prompt");

    generatedChanName = cfg.nlpName;
    generatedChanName.append(".Tokens.TXT");
}

void LangCtrl::onInit()
{
    Attach(async, serviceResponseReceived, this, onServiceResponseReceived, SkQueued);

    nlpGeneration = new SkFlowGenericSubscriber(this);
    nlpGeneration->setObjectName(this, "NlpSubscriber");
    nlpGeneration->setClient(async);

    Attach(nlpGeneration, ready, this, onNlpReady, SkOneShotQueued);
    Attach(nlpGeneration, dataAvailable, this, onNlpDataCome, SkDirect);
}

void LangCtrl::onStart()
{
}

void LangCtrl::onStop()
{
    processing = false;
}

void LangCtrl::onClose()
{
    nlpGeneration->destroyLater();
    nlpGeneration = nullptr;

    Detach(async, serviceResponseReceived, this, onServiceResponseReceived);
}

void LangCtrl::onSetConnection()
{}

void LangCtrl::onUnSetConnection()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void LangCtrl::onChannelAdded(SkFlowChannel *ch)
{
    if (!nlpGeneration)
        return;

    if (ch->name == promptServiceName)
    {
        promptServiceID = ch->chanID;
        ObjectMessage("PromptService FOUND: " << promptServiceName << "[" << promptServiceID << "]");

        nlpGeneration->subscribe(generatedChanName.c_str());
    }
}

void LangCtrl::onChannelRemoved(SkFlowChannel *ch)
{
    if (!nlpGeneration)
        return;

    if (promptServiceID > -1 && ch->chanID == promptServiceID)
    {
        ObjectMessage("PromptService REMOVED: " << promptServiceName << "[" << promptServiceID << "]");
        promptServiceID = -1;
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void LangCtrl::execPrompt(CStr *prompt)
{
    if (processing)
    {
        ObjectError("Generation is ALREADY going on ..");
        return;
    }

    if (promptServiceID < 0)
    {
        ObjectError("Nlp sat-service NOT flound: " << promptServiceName);
        return;
    }

    SkArgsMap m;
    m["prompt"] = prompt;

    SkVariant val(m);
    async->sendServiceRequest(promptServiceID, "CHAT", val);

    processing = true;
    ObjectMessage("STARTED processing prompt: " << prompt);
}

SlotImpl(LangCtrl, onServiceResponseReceived)
{
    SilentSlotArgsWarning();

    SkFlowChanID chanID = Arg_Int16;

    if (chanID != promptServiceID)
        return;

    SkVariant val = Arg_AbstractVariadic;

    SkArgsMap m;
    val.copyToMap(m);

    processing = false;

    if (m["status"].toBool())
    {
        while(!tokensQueue.isEmpty())
            generatedBlock.append(tokensQueue.dequeue());

        ObjectMessage("FINISHED processing: " << val.toString());
    }

    else
        ObjectError("ERROR while processing: " << val.toString());
}

void LangCtrl::onFlowDataCome(SkFlowChannelData &dataContainer)
{
    if (nlpGeneration && nlpGeneration->isReady() && dataContainer.chanID == nlpGeneration->source()->chanID)
        nlpGeneration->tick(dataContainer);
}

void LangCtrl::onFastTick()
{}

void LangCtrl::onSlowTick()
{}

void LangCtrl::onOneSecondTick()
{}

bool LangCtrl::isOwnedChannel(SkFlowChanID chanID)
{
    return (nlpGeneration && nlpGeneration->isReady() && chanID == nlpGeneration->source()->chanID);
}

SlotImpl(LangCtrl, onNlpReady)
{
    SilentSlotArgsWarning();
}

SlotImpl(LangCtrl, onNlpDataCome)
{
    SilentSlotArgsWarning();
    evaluateNlpData();
}

void LangCtrl::evaluateNlpData()
{
    if (nlpGeneration->lastData().isEmpty())
        return;

    SkDataBuffer &b = nlpGeneration->lastData();

    SkString txt = b.toString();
    tokensQueue.enqueue(txt);

    bool isText = false;

    for(ULong i=0; i<b.size(); i++)
    {
        if (b[i] == '\'' || b[i] == '"' || isspace(b[i]))
            continue;

        else if(!isText && isalnum(b[i]))
            isText = true;

        if (isText && ispunct(b[i]))
        {
            while(!tokensQueue.isEmpty())
                generatedBlock.append(tokensQueue.dequeue());

            onNlpBlockReady();
            break;
        }
    }
}

bool LangCtrl::isReady()
{
    return (promptServiceID > -1);
}

bool LangCtrl::isProcessing()
{
    return processing;
}

bool LangCtrl::consume(SkString &txt)
{
    if (generatedBlock.isEmpty())
        return false;

    txt = generatedBlock;
    generatedBlock.clear();

    return true;
}
