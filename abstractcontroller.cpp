#include "abstractcontroller.h"
#include <Core/App/skeventloop.h>

AbstractConstructorImpl(AbstractController, SkObject)
{
    async = nullptr;
    conn = nullptr;
    running = false;

    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecondTick);
}

void AbstractController::init(SkFlowAsync *flow)
{
    async = flow;

    ObjectWarning("Controller is initializing activities ..");
    onInit();
}

void AbstractController::start()
{
    if (running)
    {
        ObjectError("Controller is ALREADY running");
        return;
    }

    Attach(eventLoop()->fastZone_SIG, pulse, this, fastTick, SkQueued);
    Attach(eventLoop()->slowZone_SIG, pulse, this, slowTick, SkQueued);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, oneSecondTick, SkQueued);

    onStart();
    running = true;
}

void AbstractController::stop()
{
    if (!running)
    {
        ObjectError("Controller is NOT running yet");
        return;
    }

    Detach(eventLoop()->fastZone_SIG, pulse, this, fastTick);
    Detach(eventLoop()->slowZone_SIG, pulse, this, slowTick);
    Detach(eventLoop()->oneSecZone_SIG, pulse, this, oneSecondTick);

    onStop();
    running = false;
}

void AbstractController::close()
{
    ObjectWarning("Controller is closing activities ..");

    if (isRunning())
        stop();

    onClose();
}

void AbstractController::setConnection(WsConnection *connection)
{
    conn = connection;

    if (connection)
    {
        ObjectMessage("Controller ws-connection SET");
        onSetConnection();
    }

    else
    {
        ObjectMessage("Controller ws-connection UNSET");
        onUnSetConnection();
    }
}

SlotImpl(AbstractController, fastTick)
{
    SilentSlotArgsWarning();
    onFastTick();
}

SlotImpl(AbstractController, slowTick)
{
    SilentSlotArgsWarning();
    onSlowTick();
}

SlotImpl(AbstractController, oneSecondTick)
{
    SilentSlotArgsWarning();
    onOneSecondTick();
}

bool AbstractController::isRunning()
{
    return running;
}

