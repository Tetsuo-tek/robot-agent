#include "robotagent.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--agent-name",                "", "Agent",        "The name of the speeching agent");
    cli->add("--user",                      "", "",             "Flow-network username");
    cli->add("--password",                  "", "",             "Flow-network password");

    // MUST BE MULTI-DAEMONS AS FOR --asr-daemons ALSO FOR ESPEAK-SPEECH, ALL ON SATs
    cli->add("--motion-detector-name",      "", "MotionDetect", "Flow-network username for motion-detector");
    cli->add("--face-detector-name",        "", "FaceDetect",   "Flow-network username for face-detector");
    cli->add("--code-detector-name",        "", "CodeDetect",   "Flow-network username for code-detector (Bars and Qr codes");

    cli->add("--asr-daemons",               "", "",             "sets names list for asr-daemons to use (comma separated, whithout spaces)");

    cli->add("--http-listen-address",       "", "0.0.0.0",      "Http service listen address (default is 0.0.0.0)");
    cli->add("--http-listen-port",          "", "10000",        "Http service listen port (default is 10000)");
    cli->add("--max-queued-sockets",        "", "100",          "The number of max simultaneous socket acception (default is 100");
    cli->add("--ssl-cert",                  "", "",             "SSL certficate file");
    cli->add("--ssl-key",                   "", "",             "SSL key file");

    cli->add("--silence-threshold-upper",   "", "-20",          "Silence threshold upper-bound");
    cli->add("--silence-threshold-lower",   "", "-45",          "Silence threshold lower-bound");
    cli->add("--max-continuous-silence",    "", "2",            "Maximum continuous-silence duration under a record-session");
    cli->add("--mic-buffering-interval",    "", "0.1",          "Microphone buffering time-interval");
    cli->add("--max-recording-clip",        "", "10",           "Maximum recording duration, if there are continuous speaking-activity; it can be interrupted if there is a checked continuous-silence interval");

    cli->add("--speaker-language",          "", "it",           "Language identifier (default is 'it' for Italian)");
    cli->add("--speaker-gender",            "", "1",            "Voice gender, where 0=none, 1=male, 2=female (default is male)");
    cli->add("--speaker-age",               "", "40",           "Voice age, where o=none (default is 20");
    cli->add("--speaker-words-for-minutes", "", "130",          "Speaking speed in word per minute, in [80, 450] (default is 130)");
    cli->add("--speaker-words-pause",       "", "1",            "Pause between words, with units of 10 ms (default is 1)");
    cli->add("--speaker-volume",            "", "100",          "Voice volume, in [0, 100] (default is 100)");
    cli->add("--speaker-pitch",             "", "50",           "Voice pitch, in [0, 100] (default is 50)");
    cli->add("--speaker-pitch-range",       "", "40",           "Voice pitch-range, in [0, 100] (default is 40)");
    cli->add("--speaker-capitals",          "", "3",            "Announce capital letters by 0=none, 1=sound, 2=spelling, and >=3 by raising the pitch (default is 3)");

    cli->add("--nlp-name",                  "", "guest",        "Set natural-language-processor sat username (default is guest)");

    if (!cli->check())
        exit(1);

    skApp->init(2000, 100000, SK_TIMEDLOOP_RT);

    new RobotAgent;
    return skApp->exec();
}
