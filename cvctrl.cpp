﻿#include "cvctrl.h"
#include "wsmountpoint.h"

#include <Core/App/skeventloop.h>
#include <Core/Containers/skarraycast.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(CvCtrl, AbstractController)
{
    publisher = nullptr;

    motionDetectorCanvas = nullptr;
    motionDetectorBlocks = nullptr;
    faceDetectorBoxes = nullptr;
    codeDetectorMeshes = nullptr;

    watchingTo.x = -1;
    ambiguousWatchingInterval = 0.3;

    motionCanvasSent = false;
    motionBlocksSent = false;
    facesBoxesSent = false;
    codeMeshesSent = false;

    SlotSet(onPublisherReady);
    SlotSet(onSubscriberReady);
    SlotSet(onSubscriberDataCome);
}

void CvCtrl::setup(CvConfig &config)
{
    cfg = config;
}

void CvCtrl::onInit()
{
    camDecoder.setup(&f.get());

    publisher = new SkFlowVideoMJpegPublisher(this);
    publisher->setObjectName(this, "Publisher");
    publisher->setClient(async);
    publisher->setup(&y, Size(640, 360), 15, 60);

    Attach(publisher, ready, this, onPublisherReady, SkOneShotQueued);

    publisher->start();

    motionDetectorCanvas = new SkFlowGenericSubscriber(this);
    motionDetectorCanvas->setObjectName(this, "MotionCanvasSubscriber");
    motionDetectorCanvas->setClient(async);
    Attach(motionDetectorCanvas, ready, this, onSubscriberReady, SkOneShotQueued);
    Attach(motionDetectorCanvas, dataAvailable, this, onSubscriberDataCome, SkDirect);

    motionDetectorBlocks = new SkFlowGenericSubscriber(this);
    motionDetectorBlocks->setObjectName(this, "MotionBlocksSubscriber");
    motionDetectorBlocks->setClient(async);
    Attach(motionDetectorBlocks, ready, this, onSubscriberReady, SkOneShotQueued);
    Attach(motionDetectorBlocks, dataAvailable, this, onSubscriberDataCome, SkDirect);

    faceDetectorBoxes = new SkFlowGenericSubscriber(this);
    faceDetectorBoxes->setObjectName(this, "FaceBoxesSubscriber");
    faceDetectorBoxes->setClient(async);
    Attach(faceDetectorBoxes, ready, this, onSubscriberReady, SkOneShotQueued);
    Attach(faceDetectorBoxes, dataAvailable, this, onSubscriberDataCome, SkDirect);

    codeDetectorMeshes = new SkFlowGenericSubscriber(this);
    codeDetectorMeshes->setObjectName(this, "CodeMeshesSubscriber");
    codeDetectorMeshes->setClient(async);
    Attach(codeDetectorMeshes, ready, this, onSubscriberReady, SkOneShotQueued);
    Attach(codeDetectorMeshes, dataAvailable, this, onSubscriberDataCome, SkDirect);

    motionDetectorInputChanName = cfg.motionDetectorName;
    motionDetectorInputChanName.append(".Input");

    faceDetectorInputChanName = cfg.faceDetectorName;
    faceDetectorInputChanName.append(".Input");

    codeDetectorInputChanName = cfg.codeDetectorName;
    codeDetectorInputChanName.append(".Input");

    SkString chName = cfg.motionDetectorName;
    chName.append(".DetectionCanvas");
    motionDetectorCanvas->subscribe(chName.c_str());

    chName = cfg.motionDetectorName;
    chName.append(".DetectionBlocks");
    motionDetectorBlocks->subscribe(chName.c_str());

    chName = cfg.faceDetectorName;
    chName.append(".DetectionBoxes");
    faceDetectorBoxes->subscribe(chName.c_str());

    chName = cfg.codeDetectorName;
    chName.append(".DetectionMeshes");
    codeDetectorMeshes->subscribe(chName.c_str());

#if defined(ENABLE_CUDA)
    AssertKiller(!checkCudaSupport());
#endif
}

void CvCtrl::onStart()
{
    facesBoxesSent = false;
}

void CvCtrl::onStop()
{}

void CvCtrl::onClose()
{
    motionDetectorCanvas->unsubscribe();
    motionDetectorBlocks->unsubscribe();
    faceDetectorBoxes->unsubscribe();
    codeDetectorMeshes->unsubscribe();

    publisher->stop();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void CvCtrl::onSetConnection()
{}

void CvCtrl::onUnSetConnection()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void CvCtrl::addJPegData(CVoid *data, ULong sz)
{
    camDecoder.loadFromBuffer(SkArrayCast::toCStr(data), sz);

#if defined(ENABLE_CUDA)
    frame_GPU.upload(f.get());
    cv::cuda::cvtColor(frame_GPU, frame_GPU, COLOR_BGR2YUV);
    vector<GpuMat> channels;
    cv::cuda::split(frame_GPU, channels);
    channels[0].download(y.get());
#else
    vector<Mat> channels;
    f.splitToChannels(channels);
    channels[0].copyTo(y.get());
#endif

    publisher->tick();

    if (currentFaceBoxesVal.isNull() && !currentMotionCanvasVal.isNull())
    {
        if (currentFaceBoxesVal.isNull())
        {
            SkArgsMap faceBox;
            currentMotionCanvasVal.copyToMap(faceBox);

            Rect r;
            r.x = faceBox["x"].toInt();
            r.y = faceBox["y"].toInt();
            r.width = faceBox["w"].toInt();
            r.height = faceBox["h"].toInt();

            watchingTo.x = r.x + (r.width/2);
            watchingTo.y = r.y + (r.height/2);
        }
    }

    if (!currentFaceBoxesVal.isNull())
    {
        SkVariantVector rectangles;
        currentFaceBoxesVal.copyToList(rectangles);

        if (rectangles.count() == 1)
        {
            SkArgsMap faceBox;
            rectangles.first().copyToMap(faceBox);

            Rect r;
            r.x = faceBox["x"].toInt();
            r.y = faceBox["y"].toInt();
            r.width = faceBox["w"].toInt();
            r.height = faceBox["h"].toInt();

            watchingTo.x = r.x + (r.width/2);
            watchingTo.y = r.y + (r.height/2);
        }
    }

    if (!currentMotionCanvasVal.isNull() && currentMotionBlocksVal.isNull() && currentFaceBoxesVal.isNull() && currentCodeMeshesVal.isNull())
        return;

    SkArgsMap detectionsPck;

    if (!currentMotionCanvasVal.isNull())
    {
        detectionsPck["motionArea"] = currentMotionCanvasVal;
        currentMotionCanvasVal.nullify();
        motionCanvasSent = true;
    }

    if (!currentMotionBlocksVal.isNull())
    {
        detectionsPck["motionBlocks"] = currentMotionBlocksVal;
        currentMotionBlocksVal.nullify();
        motionBlocksSent = true;
    }

    if (!currentFaceBoxesVal.isNull())
    {
        detectionsPck["faces"] = currentFaceBoxesVal;
        currentFaceBoxesVal.nullify();
        facesBoxesSent = true;
    }

    if (!currentCodeMeshesVal.isNull())
    {
        detectionsPck["codes"] = currentCodeMeshesVal;
        currentCodeMeshesVal.nullify();
        codeMeshesSent = true;
    }

    SkVariant pckVal(detectionsPck);
    conn->sendCommand("DETECTION", &pckVal);
}

void CvCtrl::onFlowDataCome(SkFlowChannelData &dataContainer)
{
    SkFlowChanID chanID = dataContainer.chanID;

    if (motionDetectorCanvas && motionDetectorCanvas->isReady() && chanID == motionDetectorCanvas->source()->chanID)
        motionDetectorCanvas->tick(dataContainer);

    else if (motionDetectorBlocks && motionDetectorBlocks->isReady() && chanID == motionDetectorBlocks->source()->chanID)
        motionDetectorBlocks->tick(dataContainer);

    else if (faceDetectorBoxes && faceDetectorBoxes->isReady() && chanID == faceDetectorBoxes->source()->chanID)
        faceDetectorBoxes->tick(dataContainer);

    else if (codeDetectorMeshes && codeDetectorMeshes->isReady() && chanID == codeDetectorMeshes->source()->chanID)
        codeDetectorMeshes->tick(dataContainer);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(CvCtrl, onPublisherReady)
{
    SilentSlotArgsWarning();

    AssertKiller(!async->containsChannel(motionDetectorInputChanName.c_str())
                 || !async->containsChannel(faceDetectorInputChanName.c_str())
                 || !async->containsChannel(codeDetectorInputChanName.c_str()));

    SkFlowChanID mjpegChanID = publisher->getChannelID();

    async->attach(mjpegChanID, async->channelID(motionDetectorInputChanName.c_str()));
    async->attach(mjpegChanID, async->channelID(faceDetectorInputChanName.c_str()));
    async->attach(mjpegChanID, async->channelID(codeDetectorInputChanName.c_str()));
}

SlotImpl(CvCtrl, onSubscriberReady)
{
    SilentSlotArgsWarning();
}

SlotImpl(CvCtrl, onSubscriberDataCome)
{
    SilentSlotArgsWarning();

    SkFlowGenericSubscriber *subcriber = DynCast(SkFlowGenericSubscriber, referer);
    SkString jsonInput(SkArrayCast::toChar(subcriber->lastData().toVoid()), subcriber->lastData().size());

    if (subcriber == motionDetectorCanvas)
        currentMotionCanvasVal.fromJson(jsonInput.c_str());

    else if (subcriber == motionDetectorBlocks)
        currentMotionBlocksVal.fromJson(jsonInput.c_str());

    else if (subcriber == faceDetectorBoxes)
        currentFaceBoxesVal.fromJson(jsonInput.c_str());

    else if (subcriber == codeDetectorMeshes)
        currentCodeMeshesVal.fromJson(jsonInput.c_str());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void CvCtrl::onFastTick()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void CvCtrl::onSlowTick()
{
    if (!conn)
        return;

    if (y.isEmpty())
        return;

    SkVariantList l;

    if (watchingTo.x < 0)
    {
        if (ambiguousWatchingChrono.stop() < ambiguousWatchingInterval)
            return;

        random_device rd;
        mt19937 gen(rd());
        uniform_int_distribution<int> xyOffset(-30, 30);

        Size res = y.size();
        Point o(res.width/2, res.height/2);
        l << (o.x+xyOffset(gen)) << (o.y+xyOffset(gen));

        SkVariant watchToVal(l);
        conn->sendCommand("WATCH_TO", &watchToVal);

        uniform_real_distribution<double> newRecheckTime(0.5, 1.5);
        ambiguousWatchingInterval = newRecheckTime(gen);

        ambiguousWatchingChrono.start();
    }

    else
    {
        l << watchingTo.x << watchingTo.y;

        SkVariant watchToVal(l);
        conn->sendCommand("WATCH_TO", &watchToVal);

        watchingTo.x = -1;
        ambiguousWatchingChrono.start();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void CvCtrl::onOneSecondTick()
{
    if (!conn)
        return;

    if ((motionCanvasSent || motionBlocksSent || facesBoxesSent || codeMeshesSent) && resetDetectionChrono.stop() > 1.5)
    {
        conn->sendCommand("DETECTION_RESET");

        motionCanvasSent = false;
        motionBlocksSent = false;
        facesBoxesSent = false;
        codeMeshesSent = false;

        resetDetectionChrono.start();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool CvCtrl::isOwnedChannel(SkFlowChanID chanID)
{
    return ((motionDetectorCanvas && motionDetectorCanvas->isReady() && chanID == motionDetectorCanvas->source()->chanID)
            || (motionDetectorBlocks && motionDetectorBlocks->isReady() && chanID == motionDetectorBlocks->source()->chanID)
            || (faceDetectorBoxes && faceDetectorBoxes->isReady() && chanID == faceDetectorBoxes->source()->chanID)
            || (codeDetectorMeshes && codeDetectorMeshes->isReady() && chanID == codeDetectorMeshes->source()->chanID));
}

/*bool CvCtrl::isRunning()
{
    return running;
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_CUDA)

bool CvCtrl::checkCudaSupport()
{
    if (getCudaEnabledDeviceCount() == 0)
    {
        ObjectError("No GPU found or the library is compiled without CUDA support");
        return false;
    }

    printShortCudaDeviceInfo(cv::cuda::getDevice());
    DeviceInfo devInfo;
    SkArgsMap m;
    SkVariantList l;

    m["deviceID"] = devInfo.deviceID();
    m["name"] = devInfo.name();
    m["totalGlobalMem"] = devInfo.totalGlobalMem();
    m["sharedMemPerBlock"] = devInfo.sharedMemPerBlock();
    m["regsPerBlock"] = devInfo.regsPerBlock();
    m["warpSize"] = devInfo.warpSize();
    m["memPitch"] = devInfo.memPitch();
    m["maxThreadsPerBlock"] = devInfo.maxThreadsPerBlock();

    vect3DtoVarianList(devInfo.maxThreadsDim(), l);
    m["maxThreadsDim"] = l;

    vect3DtoVarianList(devInfo.maxGridSize(), l);
    m["maxGridSize"] = l;

    m["clockRate"] = devInfo.clockRate();
    m["totalConstMem"] = devInfo.totalConstMem();
    m["majorVersion"] = devInfo.majorVersion();
    m["minorVersion"] = devInfo.minorVersion();
    m["textureAlignment"] = devInfo.textureAlignment();
    m["texturePitchAlignment"] = devInfo.texturePitchAlignment();
    m["multiProcessorCount"] = devInfo.multiProcessorCount();
    m["kernelExecTimeoutEnabled"] = devInfo.kernelExecTimeoutEnabled();
    m["integrated"] = devInfo.integrated();
    m["canMapHostMemory"] = devInfo.canMapHostMemory();

    DeviceInfo::ComputeMode mode = devInfo.computeMode();

    //default compute mode (Multiple threads can use cudaSetDevice with this device)
    if (mode == DeviceInfo::ComputeMode::ComputeModeDefault)
        m["computeMode"] = "ComputeModeDefault";

    //compute-exclusive-thread mode (Only one thread in one process will be able to use cudaSetDevice with this device)
    else if (mode == DeviceInfo::ComputeMode::ComputeModeExclusive)
        m["computeMode"] = "ComputeModeExclusive";

    //compute-prohibited mode (No threads can use cudaSetDevice with this device)
    else if (mode == DeviceInfo::ComputeMode::ComputeModeProhibited)
        m["computeMode"] = "ComputeModeProhibited";

    //compute-exclusive-process mode (Many threads in one process will be able to use cudaSetDevice with this device)
    else if (mode == DeviceInfo::ComputeMode::ComputeModeExclusiveProcess)
        m["computeMode"] = "ComputeModeExclusiveProcess";

    m["maxTexture1D"] = devInfo.maxTexture1D();
    m["maxTexture1DMipmap"] = devInfo.maxTexture1DMipmap();
    m["maxTexture1DLinear"] = devInfo.maxTexture1DLinear();

    vect2DtoVarianList(devInfo.maxTexture2D(), l);
    m["maxTexture2D"] = l;

    vect2DtoVarianList(devInfo.maxTexture2DMipmap(), l);
    m["maxTexture2DMipmap"] = l;

    vect3DtoVarianList(devInfo.maxTexture2DLinear(), l);
    m["maxTexture2DLinear"] = l;

    vect2DtoVarianList(devInfo.maxTexture2DGather(), l);
    m["maxTexture2DGather"] = l;

    vect3DtoVarianList(devInfo.maxTexture3D(), l);
    m["maxTexture3D"] = l;

    m["maxTextureCubemap"] = devInfo.maxTextureCubemap();

    vect2DtoVarianList(devInfo.maxTexture1DLayered(), l);
    m["maxTexture1DLayered"] = l;

    vect3DtoVarianList(devInfo.maxTexture2DLayered(), l);
    m["maxTexture2DLayered"] = l;

    vect2DtoVarianList(devInfo.maxTextureCubemapLayered(), l);
    m["maxTextureCubemapLayered"] = l;

    m["maxSurface1D"] = devInfo.maxSurface1D();

    vect2DtoVarianList(devInfo.maxSurface2D(), l);
    m["maxSurface2D"] = l;

    vect3DtoVarianList(devInfo.maxSurface3D(), l);
    m["maxSurface3D"] = l;

    vect2DtoVarianList(devInfo.maxSurface1DLayered(), l);
    m["maxSurface1DLayered"] = l;

    vect3DtoVarianList(devInfo.maxSurface2DLayered(), l);
    m["maxSurface2DLayered"] = l;

    m["maxSurfaceCubemap"] = devInfo.maxSurfaceCubemap();

    vect2DtoVarianList(devInfo.maxSurfaceCubemapLayered(), l);
    m["maxSurfaceCubemapLayered"] = l;

    m["surfaceAlignment"] = devInfo.surfaceAlignment();
    m["concurrentKernels"] = devInfo.concurrentKernels();
    m["ECCEnabled"] = devInfo.ECCEnabled();
    m["pciBusID"] = devInfo.pciBusID();
    m["pciDeviceID"] = devInfo.pciDeviceID();
    m["pciDomainID"] = devInfo.pciDomainID();
    m["tccDriver"] = devInfo.tccDriver();
    m["asyncEngineCount"] = devInfo.asyncEngineCount();
    m["unifiedAddressing"] = devInfo.unifiedAddressing();
    m["memoryClockRate"] = devInfo.memoryClockRate();
    m["memoryBusWidth"] = devInfo.memoryBusWidth();
    m["l2CacheSize"] = devInfo.l2CacheSize();
    m["maxThreadsPerMultiProcessor"] = devInfo.maxThreadsPerMultiProcessor();

    l.clear();

    if (deviceSupports(FEATURE_SET_COMPUTE_10))
        l << "FEATURE_SET_COMPUTE_10";

    if (deviceSupports(FEATURE_SET_COMPUTE_11))
        l << "FEATURE_SET_COMPUTE_11";

    if (deviceSupports(FEATURE_SET_COMPUTE_12))
        l << "FEATURE_SET_COMPUTE_12";

    if (deviceSupports(FEATURE_SET_COMPUTE_13))
        l << "FEATURE_SET_COMPUTE_13";

    if (deviceSupports(FEATURE_SET_COMPUTE_20))
        l << "FEATURE_SET_COMPUTE_20";

    if (deviceSupports(FEATURE_SET_COMPUTE_21))
        l << "FEATURE_SET_COMPUTE_21";

    if (deviceSupports(FEATURE_SET_COMPUTE_30))
        l << "FEATURE_SET_COMPUTE_30";

    if (deviceSupports(FEATURE_SET_COMPUTE_32))
        l << "FEATURE_SET_COMPUTE_32";

    if (deviceSupports(FEATURE_SET_COMPUTE_35))
        l << "FEATURE_SET_COMPUTE_35";

    if (deviceSupports(FEATURE_SET_COMPUTE_50))
        l << "FEATURE_SET_COMPUTE_50";

    if (deviceSupports(GLOBAL_ATOMICS))
        l << "GLOBAL_ATOMICS";

    if (deviceSupports(SHARED_ATOMICS))
        l << "SHARED_ATOMICS";

    if (deviceSupports(NATIVE_DOUBLE))
        l << "NATIVE_DOUBLE";

    if (deviceSupports(WARP_SHUFFLE_FUNCTIONS))
        l << "WARP_SHUFFLE_FUNCTIONS";

    if (deviceSupports(DYNAMIC_PARALLELISM))
        l << "DYNAMIC_PARALLELISM";

     m["featuresSupported"] = l;

    async->setVariable("CV-CUDA", m);

    SkString json;
    m.toString(json, true);

    ObjectWarning("Opencv CUDA support CHECKED: " << json);

    return true;
}

void CvCtrl::vect2DtoVarianList(Vec2i vect, SkVariantList &l)
{
    l.clear();
    l << vect[0];
    l << vect[1];
}

void CvCtrl::vect3DtoVarianList(Vec3i vect, SkVariantList &l)
{
    l.clear();
    l << vect[0];
    l << vect[1];
    l << vect[2];
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
