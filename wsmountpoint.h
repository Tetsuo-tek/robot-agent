#ifndef WSMOUNTPOINT_H
#define WSMOUNTPOINT_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h>
#include <Core/System/Network/FlowNetwork/skflowasync.h>
#include <Core/System/Time/skelapsedtime.h>

#include "avatarctrl.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class WsConnection extends SkObject
{
    SkFlowAsync *async;
    SkWebSocket *sck;
    AvatarCtrl *avtCtrl;

    SkElapsedTime pingChrono;

    public:
        Constructor(WsConnection, SkObject);

        void setup(SkFlowAsync *flow,
                   SkWebSocket *socket,
                   AvatarCtrl *avatarCtrl);

        void sendCommand(CStr *cmd, SkVariant *dataVal=nullptr);
        void sendData(SkDataBuffer &data);
        void close();

        Slot(onTextReadyRead);
        Slot(onDataReadyRead);
        Slot(onDisconnected);

        Slot(onFastTick);
        Slot(onSlowTick);
        Slot(onOneSecondTick);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct WsMountpointConfig
{
    SkString svrName;
    SkString address;
    int port;
    int maxQueuedSockets;
    bool enableSsl;
    SkString certificate;
    SkString key;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkHttpService;

class WsMountpoint extends SkGenericMountPoint
{
    WsMountpointConfig cfg;
    SkHttpService *service;

    SkFlowAsync *async;
    WsConnection *conn;

    SkFlowChannel *faceDetectorInputChan;
    SkFlowChannel *faceDetectedBoxesChan;

    AvatarCtrl *avtCtrl;

    public:
        Constructor(WsMountpoint, SkGenericMountPoint);

        bool open(SkFlowAsync *flow, WsMountpointConfig &config, AvatarCtrl *avatarCtrl);
        void close();

        Slot(onUserAccepted);
        Slot(onUserDisconnected);

        bool enabled();
        WsConnection *connection();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // WSMOUNTPOINT_H
