#ifndef ABSTRACTCONTROLLER_H
#define ABSTRACTCONTROLLER_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Network/FlowNetwork/skflowasync.h>

class WsConnection;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class AbstractController extends SkObject
{
    bool running;

    public:
        void init(SkFlowAsync *flow);
        void start();
        void stop();
        void close();

        void setConnection(WsConnection *connection);

        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecondTick);

        virtual bool isOwnedChannel(SkFlowChanID)               = 0;
        virtual void onFlowDataCome(SkFlowChannelData &)        = 0;

        bool isRunning();

    protected:
        SkFlowAsync *async;
        WsConnection *conn;

        AbstractConstructor(AbstractController, SkObject);

        virtual void onInit()                                   = 0;
        virtual void onStart()                                  = 0;
        virtual void onStop()                                   = 0;
        virtual void onClose()                                  = 0;
        virtual void onSetConnection()                          = 0;
        virtual void onUnSetConnection()                        = 0;
        virtual void onFastTick()                               = 0;
        virtual void onSlowTick()                               = 0;
        virtual void onOneSecondTick()                          = 0;
};

#endif // ABSTRACTCONTROLLER_H
