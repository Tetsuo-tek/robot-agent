# Robot Agent tool

It is a distributed and medium-complex satellite application that uses other secondary satellites to offer a virtual assistant service capable of listening, seeing, and operating procedures and commands on its system or other remote satellites. All agent activities flow through [SkRobot](https://gitlab.com/Tetsuo-tek/SkRobot). Robot agent is developed in C++ under [SpecialK](https://gitlab.com/Tetsuo-tek/SpecialK) framework.

An application screenshot:

![Screenshot](doc/agent-screenshot.png){style="display: block; margin: 0 auto"}

Consider robot-agent as an educational experiment acting as a demo related to SkRobot and  FlowNetwork.

In this case, SkRobot application load core detection modules for faces, movements, and QR/Bar codes and collaborates with external satellites for speech-recognition services using local OpenAI Whisper technology.

It's supposed an Nvidia GPU with adequate memory is available. In that case, the application leverages CUDA to enhance processing capabilities, significantly boosting the performance of Automatic Speech Recognition (ASR) and computer vision tasks using cv::cuda.

The system has been successfully developed and tested on Nvidia's Jetson Orin AGX and Jetson Nano B01 platforms.

Its user interface is web-based and compatible with major browsers like Chrome, Chromium, and Edge, although Firefox is excluded due to its lack of support for audio resampling. Upon accessing SkRobot through a web browser, the HTML, JavaScript, and CSS required for the interface are downloaded.

The JavaScript initialises a WebSocket client that connects to the robot-agent application on the same IP, facilitating real-time interaction. Once the connection handshake is complete, the application displays a 3D avatar of the virtual assistant, based on the open-source ["Armanda - 3D Talking Agent"](https://github.com/PatrizioM/armanda) by Patrizio Migliarini, PhD, which has been adapted and integrated into the SkRobot environment.

The robot agent FlowNetwork graph:

![Agent Graph](doc/AgentGraph.png){style="display: block; margin: 0 auto"}


The SkRobot web-application allows users to activate their microphone and camera through the browser, using interface buttons and granting the required multimedia input permissions. Audio and video streams are captured and transmitted over WebSocket.

The Audio Stream is sent as a binary sequence of 16-bit PCM buffers, mono, at a sampling rate of 22050 Hz.

The Video Stream is sent as 640x360 resolution JPEG frames at 15 frames per second (MJPEG).

The browser-based WebSocket client receives JSON text commands to control the avatar and associated actions, including eyes blinking, gaze direction, and lips movement synchronised with synthetic speech from the Espeak library.

Despite Espeak's simplistic and robotic vocal timbre, it supports Italian and operates efficiently on lower-performance devices. Binary PCM audio data representing the avatar's voice is transmitted back to the browser, with lips movements triggered by phonetic analysis performed by the Espeak engine, timed to match the speech.

For network operations related to Video Processing, the original video frame is converted to a monochrome JPEG of just the Y channel (most significant for detection) for lighter processing.

For Audio Processing, additional audio channels are created for the Fast Fourier Transform (FFT) of the microphone audio and ASR daemon control. Each collaborating satellite for speech recognition, named robot-whisper-asr \cite{diottavio2023robotwhisperasr}, manages two channels: Audio Input for downloading audio from the microphone and transcription output for uploading transcribed text. The transcription daemon subscribes to the Agent.ctrl control queue and multiple daemons can operate concurrently to enhance responsiveness, even during active transcription phases.

Detector modules process and emit detection data (bounding geometries) through various queues. This data is aggregated and sent over WebSocket to the browser, where JavaScript visualises detected geometries overlaid on the camera preview.

Facial expressions are implemented to enhance realism so that the avatar's blinking rate varies from 450 ms to 4 seconds, with distinct speeds for each blink phase. Also, Gaze Direction is controlled via data from face detection or the centre of motion if no faces are detected.

This architecture allows for scalable parallel processing for ASR and detection services, optimising the system’s efficiency and responsiveness. In this demo, the browser primarily acts as a slave-executor, controlled by the application through pseudo-real-time remote commands. The only autonomous browser functions manage the FPS for camera capture and transitions during the blinking phase.