#include "avatarctrl.h"
#include <Core/App/skeventloop.h>
#include <Core/Containers/skarraycast.h>
#include <Core/System/Filesystem/skfsutils.h>
#include <Core/System/Filesystem/skfileinfoslist.h>

#include "wsmountpoint.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define PROCEDURES_DIR      "procedures.d/"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(AvatarCtrl, SkAbstractWorkerObject)
{
    running = false;
    conn = nullptr;
    async = nullptr;

    asrCtrl = nullptr;
    spkCtrl = nullptr;
    cvCtrl = nullptr;
    langCtrl = nullptr;

    frontendReady = false;

    audioEnabled = false;
    micEnabled = false;
    camEnabled = false;

    randomBlinkInterval = 0;

    speeching = false;

    SlotSet(onFastTick);
    SlotSet(onSlowTick);
    SlotSet(onOneSecondTick);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AvatarCtrl::init(AvatarConfig &config, SkFlowAsync *flow)
{
    cfg = config;
    async = flow;

    asrCtrl = new AsrCtrl(this);
    asrCtrl->setObjectName(this, "AsrCtrl");
    asrCtrl->setup(cfg.asrCfg);
    asrCtrl->init(async);

    spkCtrl = new SpkCtrl(this);
    spkCtrl->setObjectName(this, "SpkCtrl");
    spkCtrl->setup(cfg.spkCfg);
    spkCtrl->init(async);

    cvCtrl = new CvCtrl(this);
    cvCtrl->setObjectName(this, "CvCtrl");
    cvCtrl->setup(cfg.cvCfg);
    cvCtrl->init(async);

    langCtrl = new LangCtrl(this);
    langCtrl->setObjectName(this, "LangCtrl");
    langCtrl->setup(cfg.langCfg);
    langCtrl->init(async);
}

void AvatarCtrl::start()
{
    if (running)
    {
        ObjectError("Control is ALREADY running");
        return;
    }

    running = true;

    Attach(eventLoop()->fastZone_SIG, pulse, this, onFastTick, SkQueued);
    Attach(eventLoop()->slowZone_SIG, pulse, this, onSlowTick, SkQueued);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecondTick, SkQueued);
}

void AvatarCtrl::stop()
{
    if (!running)
    {
        ObjectError("Control is NOT running");
        return;
    }

    running = false;

    if (asrCtrl->isRunning())
        asrCtrl->stop();

    if (spkCtrl->isRunning())
        spkCtrl->stop();

    if (cvCtrl->isRunning())
        cvCtrl->stop();

    if (langCtrl->isRunning())
        langCtrl->stop();

    Detach(eventLoop()->fastZone_SIG, pulse, this, onFastTick);
    Detach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecondTick);
}

void AvatarCtrl::close()
{
    ObjectWarning("Closing .. ");

    asrCtrl->close();
    asrCtrl->destroyLater();
    asrCtrl = nullptr;

    spkCtrl->close();
    spkCtrl->destroyLater();
    spkCtrl = nullptr;

    cvCtrl->close();
    cvCtrl->destroyLater();
    cvCtrl = nullptr;

    langCtrl->close();
    langCtrl->destroyLater();
    langCtrl = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AvatarCtrl::setConnection(WsConnection *connection)
{
    conn = connection;

    if (!conn && isRunning())
        stop();

    if (!conn)
    {
        asrCtrl->setConnection(nullptr);
        spkCtrl->setConnection(nullptr);
        cvCtrl->setConnection(nullptr);
        langCtrl->setConnection(nullptr);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AvatarCtrl::onChannelAdded(SkFlowChannel *ch)
{
    if (asrCtrl)
        asrCtrl->onChannelAdded(ch);

    if (langCtrl)
        langCtrl->onChannelAdded(ch);
}

void AvatarCtrl::onChannelRemoved(SkFlowChannel *ch)
{
    if (asrCtrl)
        asrCtrl->onChannelRemoved(ch);

    if (langCtrl)
        langCtrl->onChannelRemoved(ch);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AvatarCtrl::onCamDataCome(CVoid *data, ULong sz)
{
    if (cvCtrl)
        cvCtrl->addJPegData(data, sz);
}

void AvatarCtrl::onMicDataCome(CVoid *data, ULong sz)
{
    if (asrCtrl && micEnabled && !speeching)
        asrCtrl->addPcmData(data, sz);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AvatarCtrl::onWsConnEvtCmdCome(SkArgsMap &evt)
{
    SkString dvt = evt["type"].toString();

    if (dvt == "READY")
    {
        ObjectMessage("Frontend is READY");
        frontendReady = true;

        start();

        asrCtrl->setConnection(conn);
        spkCtrl->setConnection(conn);
        cvCtrl->setConnection(conn);
        langCtrl->setConnection(conn);
    }

    else if (dvt == "AUDIO_ON")
    {
        ObjectMessage("Frontend has switched-ON audio");
        audioEnabled = true;

        if (!spkCtrl->isRunning())
            spkCtrl->start();
    }

    else if (dvt == "AUDIO_OFF")
    {
        ObjectMessage("Frontend has switched-OFF audio");

        audioEnabled = false;

        if (spkCtrl->isRunning())
            spkCtrl->stop();

        if (asrCtrl->isRunning())
            asrCtrl->stop();

        if (langCtrl->isRunning())
            langCtrl->stop();

        if (!speechBlocks.isEmpty())
            speechBlocks.clear();
    }

    else if (dvt == "SPEECH_STARTED")
    {
        ObjectMessage("Avatar-UI has STARTED to speak");
        //speeching = true; <- directly on speech request
    }

    else if (dvt == "SPEECH_FINISHED")
    {
        ObjectMessage("Avatar-UI has FINISHED to speak");
        speeching = false;
    }

    else if (dvt == "LOAD_PHONEME_LABIAL")
    {
        SkString name = evt["args"].toString();

        SkString e;
        evt.toString(e, true);
        cout << e << "\n";
        spkCtrl->onPhonemeLabialRequest(name.c_str());
    }

    else if (dvt == "SAVE_PHONEME_LABIAL")
    {
        SkArgsMap phonemeLabial;
        evt["args"].copyToMap(phonemeLabial);
        spkCtrl->setPhonemeLabial(phonemeLabial);
    }

    else if (dvt == "SPKR_TEST")
    {
        ObjectMessage("Frontend has requested for a speaker-test");
        spkCtrl->makeVoiceTest();
    }

    else if (dvt == "MIC_ON")
    {
        ObjectMessage("Frontend has switched-ON the microphone");

        micEnabled = true;
        asrCtrl->start();
    }

    else if (dvt == "MIC_OFF")
    {
        ObjectMessage("Frontend has switched-OFF the microphone");

        micEnabled = false;
        asrCtrl->stop();
    }

    else if (dvt == "CAM_ON")
    {
        ObjectMessage("Frontend has switched-ON the camera");

        camEnabled = true;
        cvCtrl->start();
    }

    else if (dvt == "CAM_OFF")
    {
        ObjectMessage("Frontend has switched-OFF the camera");

        camEnabled = false;
        cvCtrl->stop();
    }

    else if (dvt == "GOTTEN_FOCUS")
        ObjectMessage("Frontend has GOTTEN focus");

    else if (dvt == "LOST_FOCUS")
        ObjectMessage("Frontend has LOST focus");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AvatarCtrl::onFlowDataCome(SkFlowChannelData &dataContainer)
{
    if (cvCtrl->isOwnedChannel(dataContainer.chanID))
        cvCtrl->onFlowDataCome(dataContainer);

    else if (asrCtrl->isOwnedChannel(dataContainer.chanID))
    {
        asrCtrl->onFlowDataCome(dataContainer);
        analyze(asrCtrl->lastTranscription());
    }

    else if (langCtrl->isOwnedChannel(dataContainer.chanID))
        langCtrl->onFlowDataCome(dataContainer);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AvatarCtrl::analyze(SkArgsMap &transcription)
{
    SkString txt = transcription["text"].toString();
    txt.trim();

    if (txt.isEmpty() || txt == "Sottotitoli e revisione a cura di QTSS"
            || txt == "Sottotitoli creati dalla comunità Amara.org")
    {
        ObjectWarning("Ignoring BIASED misunderstanding trascription: " << txt);
        return;
    }

    ObjectMessage("Analyzing transcription from AsrDaemon: " << txt);
    txt.toLowerCase();

    if (txt.isEmpty())
        return;

    /*SkString txtWithoutPuncts;

    for(ULong i=0; i < txt.size(); i++)
    {
        char c = txt[i];

        if (::isalpha(c) || ::isspace(c)) //|| ::isdigit(c)
            txtWithoutPuncts += c;
    }

    SkStringList tokens;
    txtWithoutPuncts.split(" ", tokens);

    if (tokens.contains("grazie"))
    {
        ObjectWarning("Ignoring BIASED misunderstanding trascription: " << txt);
        return;
    }

    static bool testEnabled = false;

    if (tokens.contains("procedura"))
    {
        if (tokens.contains("ferma"))
        {
            testEnabled = false;
            ObjectWarning("Procedure STOPPED");
            spkCtrl->speech("Ok, fermo la procedura attiva!");
            return;
        }

        else
        {
            if (tokens.contains("ripeti"))
            {
                testEnabled = true;
                ObjectWarning("Procedure STARTED");
                spkCtrl->speech("Hai scelto la procedura di eco vocale. Questo implica che ripeterò quello che tu dirai; puoi iniziare  ..");
                return;
            }
        }
    }

    if (testEnabled)
        spkCtrl->speech(txt.c_str());*/

    if (langCtrl && langCtrl->isReady())
    {
        langCtrl->start();
        langCtrl->execPrompt(txt.c_str());
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AvatarCtrl::updateCommand(SkWorkerCommand *)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(AvatarCtrl, onFastTick)
{
    SilentSlotArgsWarning();

    if (!running || !conn || !frontendReady)
        return;

    if (eyesBlinkChrono.stop() >= randomBlinkInterval)
    {
        random_device rd;
        mt19937 generator(rd());
        int minMs = 200;
        int maxMs = 4000;
        uniform_int_distribution<int> distribution(minMs, maxMs);
        randomBlinkInterval = distribution(generator);

        conn->sendCommand("EYE_BLINK");
        eyesBlinkChrono.start();
    }

    if (langCtrl)
    {
        if (audioEnabled)//if (langCtrl->isRunning())
        {
            SkString generatedText;

            if (langCtrl->consume(generatedText))
                speechBlocks.enqueue(generatedText);
        }
    }
}

SlotImpl(AvatarCtrl, onSlowTick)
{
    if (!speeching)
    {
        if (speechBlocks.isEmpty())
        {
            if (langCtrl && langCtrl->isRunning() && !langCtrl->isProcessing())
                langCtrl->stop();
        }

        else
        {
            if (audioEnabled)
            {
                speeching = true;
                SkString txt = speechBlocks.dequeue();
                spkCtrl->speech(txt.c_str());
            }

            else
                speechBlocks.clear();
        }
    }
}

SlotImpl(AvatarCtrl, onOneSecondTick)
{
    SilentSlotArgsWarning();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool AvatarCtrl::isRunning()
{
    return running;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
