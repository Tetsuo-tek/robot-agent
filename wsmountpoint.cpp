#include "wsmountpoint.h"

#include <Core/App/skeventloop.h>
#include <Core/Containers/skarraycast.h>
#include <Core/System/Network/TCP/HTTP/Server/skhttpservice.h>
#include <Core/System/Network/TCP/HTTP/skwebsocket.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(WsConnection, SkObject)
{
    async = nullptr;
    sck = nullptr;
    avtCtrl = nullptr;

    SlotSet(onTextReadyRead);
    SlotSet(onDataReadyRead);
    SlotSet(onDisconnected);

    SlotSet(onFastTick);
    SlotSet(onSlowTick);
    SlotSet(onOneSecondTick);

    Attach(eventLoop()->fastZone_SIG, pulse, this, onFastTick, SkQueued);
    Attach(eventLoop()->slowZone_SIG, pulse, this, onSlowTick, SkQueued);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecondTick, SkQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WsConnection::setup(SkFlowAsync *flow,
                         SkWebSocket *socket,
                         AvatarCtrl *avatarCtrl)
{
    async = flow;
    sck = socket;
    avtCtrl = avatarCtrl;

    Attach(sck, textReadyRead, this, onTextReadyRead, SkQueued);
    Attach(sck, binaryReadyRead, this, onDataReadyRead, SkQueued);
    Attach(sck, disconnected, this, onDisconnected, SkDirect);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WsConnection::sendCommand(CStr *cmd, SkVariant *dataVal)
{
    if (!sck)
        return;

    SkString json;
    SkArgsMap cmdMap;
    cmdMap["cmd"] = cmd;

    if (dataVal)
        cmdMap["data"] = *dataVal;

    cmdMap.toString(json);

    sck->setSendingMode(WSM_TEXT);
    sck->write(json);
}

void WsConnection::sendData(SkDataBuffer &data)
{
    if (!sck)
        return;

    sck->setSendingMode(WSM_BINARY);
    sck->write(&data);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WsConnection::close()
{
    if (sck)
        sck->disconnect();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(WsConnection, onTextReadyRead)
{
    SilentSlotArgsWarning();

    if (!sck || !sck->isConnected())
        return;

    SkString json;
    sck->read(json);

    SkArgsMap evt;

    if (!evt.fromString(json.c_str()))
        return;

    avtCtrl->onWsConnEvtCmdCome(evt);
}

SlotImpl(WsConnection, onDataReadyRead)
{
    SilentSlotArgsWarning();

    if (!sck || !sck->isConnected())
        return;

    SkDataBuffer b;
    sck->read(&b);

    if (SkString::compare(SkArrayCast::toChar(b.toVoid()), " CAM", 4))
    {
        //cout << "CAM " << (b.size()-4) << "\n";
        avtCtrl->onCamDataCome(&b.data()[4], b.size()-4);
    }

    else if (SkString::compare(SkArrayCast::toChar(b.toVoid()), " MIC", 4))
    {
        //cout << "MIC " <<( b.size()-4) << "\n";
        avtCtrl->onMicDataCome(&b.data()[4], b.size()-4);
    }
}

SlotImpl(WsConnection, onDisconnected)
{
    SilentSlotArgsWarning();

    sck = nullptr;

    ObjectMessage("REMOVED socket");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(WsConnection, onFastTick)
{
    SilentSlotArgsWarning();
}

SlotImpl(WsConnection, onSlowTick)
{
    SilentSlotArgsWarning();
}

SlotImpl(WsConnection, onOneSecondTick)
{
    SilentSlotArgsWarning();

    if (!sck)
        return;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(WsMountpoint, SkGenericMountPoint)
{
    service = nullptr;
    async = nullptr;
    avtCtrl = nullptr;

    conn = nullptr;

    SlotSet(onUserAccepted);
    SlotSet(onUserDisconnected);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool WsMountpoint::open(SkFlowAsync *flow, WsMountpointConfig &config, AvatarCtrl *avatarCtrl)
{
    cfg = config;
    async = flow;
    avtCtrl = avatarCtrl;

    service = new SkHttpService(parent());
    service->setObjectName(this, "Service");
    service->setup(objectName(), true, 8192, 10);
    service->sslSvr()->initSslCtx(cfg.certificate.c_str(), cfg.key.c_str());

    AssertKiller(!setup("/agent", true) || !service->addMountPoint(this));
    Attach(this, accepted, this, onUserAccepted, SkQueued);

    return service->start(cfg.address.c_str(), cfg.port, cfg.maxQueuedSockets);
}

void WsMountpoint::close()
{
    service->stop();

    if (conn)
    {
        conn->close();
        conn = nullptr;
    }

    Detach(this, accepted, this, onUserAccepted);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(WsMountpoint, onUserAccepted)
{
    SilentSlotArgsWarning();

    SkWebSocket *wsSck = dynamic_cast<SkWebSocket *>(referer);

    if (!wsSck)
    {
        wsSck->disconnect();
        ObjectError("Connection is NOT WebSocket");
        return;
    }

    if (conn)
    {
        wsSck->disconnect();
        ObjectError("An agent user-connection is ALREADY active: " << conn->objectName());
        return;
    }

    conn = new WsConnection(this);
    conn->setObjectName(this, "WsInterface");
    conn->setup(async, wsSck/*, faceDetectorInputChan, faceDetectedBoxesChan, asrCtrl*/, avtCtrl);

    Attach(wsSck, disconnected, this, onUserDisconnected, SkDirect);

    avtCtrl->setConnection(conn);

    ObjectMessage("ADDED Agent user-connection: " << conn->objectName());
}

SlotImpl(WsMountpoint, onUserDisconnected)
{
    SilentSlotArgsWarning();

    ObjectMessage("REMOVED Agent user-connection");
    conn = nullptr;
    avtCtrl->setConnection(nullptr);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool WsMountpoint::enabled()
{
    return (conn != nullptr);
}

WsConnection *WsMountpoint::connection()
{
    return conn;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
