#include "asrctrl.h"
#include "wsmountpoint.h"

#include "Core/App/skeventloop.h"
#include <Core/Containers/skarraycast.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(AsrCtrl, AbstractController)
{
    publisher = nullptr;
    currentRecordingDaemon = nullptr;

    st = Asr_DISABLED;
    connectedDaemons = 0;
    processingDaemons = 0;
    vuCurrentValue = -1000.f;
    daemonCtrlChan = -1;
    silenceCheckStarted = false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AsrCtrl::setup(AsrConfig &config)
{
    cfg = config;
}

void AsrCtrl::onInit()
{
    params.set(true, 1, 22050, 2048, AFMT_SHORT);

    publisher = new SkFlowAudioPublisher(this);
    publisher->setObjectName(this, "Publisher");
    publisher->setClient(async);
    publisher->setup(params);

    inputBuffer.setObjectName(this, "PCM");
    publisher->setInputBuffer(&inputBuffer);
    publisher->enableFftGaussianFilter(true);
    publisher->start();

    vuCheckerAudioBuffer.setObjectName(this, "VuAvgChecker");
    vuCheckerAudioBuffer.setAudioParameters(params, &inputBuffer);

    for(ULong i=0; i<cfg.asrDaemonsNames.count(); i++)
    {
        AsrDaemon *d = new AsrDaemon;

        d->name = cfg.asrDaemonsNames[i];
        d->st = AsrD_OFFLINE;

        d->micChanName = d->name;
        d->micChanName.append(".PcmInput");
        d->micChan = nullptr;

        d->transcriptionChanName = d->name;
        d->transcriptionChanName.append(".Transcription");
        d->transcriptionChan = nullptr;

        daemons[d->name] = d;
    }

    async->addStreamingChannel(daemonCtrlChan, FT_CTRL_SWITCH, T_MAP, "Ctrl");

    st = Asr_DISABLED;
}

void AsrCtrl::onStart()
{
    inputBuffer.clear();
    silenceCheckStarted = false;
    vuCurrentValue = -1000.f;

    if (conn && st == Asr_IDLE)
        conn->sendCommand("ASR_IDLE", nullptr);

    else
        conn->sendCommand("ASR_BUSY", nullptr);
}

void AsrCtrl::onStop()
{}

void AsrCtrl::onClose()
{
    publisher->stop();

    SkBinaryTreeVisit<SkString, AsrDaemon *> *itr = daemons.iterator();

    while(itr->next())
        delete itr->item().value();

    delete itr;

    daemons.clear();
    pcmDaemonsChan.clear();
    transcriptionDaemonsChan.clear();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AsrCtrl::onSetConnection()
{}

void AsrCtrl::onUnSetConnection()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AsrCtrl::onChannelAdded(SkFlowChannel *ch)
{
    SkStringList parsed;
    ch->name.split(".", parsed);

    if (!daemons.contains(parsed.first()))
        return;

    AsrDaemon *d = daemons[parsed.first()];

    if (ch->name == d->micChanName)
    {
        d->st = AsrD_IDLE;

        connectedDaemons++;

        if (checkForChangeToIDLE())
            ObjectWarning("READY to capture");

        d->micChan = ch;
        pcmDaemonsChan[ch->chanID] = d;

        ObjectWarning("CHECKED daemon pcm inputBuffer-channel: " << d->micChanName);
    }

    else if (ch->name == d->transcriptionChanName)
    {
        d->transcriptionChan = ch;
        transcriptionDaemonsChan[ch->chanID] = d;
        ObjectWarning("CHECKED daemon transcription output-channel: " << d->transcriptionChanName);
    }
}

void AsrCtrl::onChannelRemoved(SkFlowChannel *ch)
{
    SkStringList parsed;
    ch->name.split(".", parsed);

    if (!daemons.contains(parsed.first()))
        return;

    AsrDaemon *d = daemons[parsed.first()];

    if (d->st != AsrD_OFFLINE)
    {
        pcmDaemonsChan.remove(d->micChan->chanID);
        transcriptionDaemonsChan.remove(d->transcriptionChan->chanID);

        d->st = AsrD_OFFLINE;
        d->micChan = nullptr;
        d->transcriptionChan = nullptr;

        connectedDaemons--;

        if (checkForChangeToBUSY())
            ObjectWarning("Asr daemon is DOWN (there are NOT active daemons): " << d->name);

        else
            ObjectWarning("Asr daemon is DOWN: " << d->name);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AsrCtrl::addPcmData(CVoid *data, ULong sz)
{
    if (st == Asr_RECORDING)
        inputBuffer.addData(SkArrayCast::toCStr(data), sz);

    SkDataBuffer *pcm = new SkDataBuffer;
    pcm->setData(data, sz);

    inputPcmWindow.enqueue(pcm);

    vuCheckerAudioBuffer.tick(SkArrayCast::toChar(pcm->toVoid()));
    vuCurrentValue = vuCheckerAudioBuffer.getLastMonoVolumeUnits();
    tick();

    float queueDuration = inputPcmWindow.count()*params.getTickTimePeriod();

    if (queueDuration > cfg.bufferingingInterval)
    {
        /*vuCurrentValue = 0.f;
        SkAbstractListIterator<SkDataBuffer *> *itr = inputPcmWindow.getInternalList().iterator();

        while(itr->next())
        {
            vuCheckerAudioBuffer.tick(SkArrayCast::toChar(itr->item()->toVoid()));
            vuCurrentValue += vuCheckerAudioBuffer.getLastMonoVolumeUnits();
        }

        delete itr;
        vuCurrentValue /= inputPcmWindow.count();

        tick();*/

        SkDataBuffer *b = inputPcmWindow.dequeue();
        delete b;
    }
}

void AsrCtrl::onFlowDataCome(SkFlowChannelData &dataContainer)
{
    AsrDaemon *d = transcriptionDaemonsChan[dataContainer.chanID];

    transcription.clear();

    SkString json(SkArrayCast::toChar(dataContainer.data.toVoid()), dataContainer.data.size());
    transcription.fromString(json.c_str());

    async->unsubscribeChannel(d->transcriptionChan);
    processingDaemons--;

    json.clear();
    transcription.toString(json, true);
    ObjectMessage("Transcription grabbed from daemon '" << d->name << "' [processing -> "
                                                        << processingDaemons << "/" << connectedDaemons << "]: "
                                                        << json);

    if (checkForChangeToIDLE())
        ObjectWarning("READY to capture");

    d->st = AsrD_IDLE;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AsrCtrl::onFastTick()
{
    if (!isRunning())
        return;

    if (inputBuffer.isEmpty())
        return;

    publisher->tick();
}

void AsrCtrl::onSlowTick()
{
    cout << "[Vu] (" << cfg.silenceThresholdLower << ", " << cfg.silenceThresholdUpper<< ")-> " << vuCurrentValue << " db                                \r"; cout.flush();
}

void AsrCtrl::onOneSecondTick()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AsrCtrl::startCurrentRecording()
{
    //IT CANNOT BE nullptr
    currentRecordingDaemon = selectNextAsrDaemon();
    AssertKiller(!currentRecordingDaemon);
    processingDaemons++;

    ObjectMessage("Speaking-activity DETECTED; selected NEXT transcription-daemon [processing -> "
                  << processingDaemons << "/" << connectedDaemons << "]: "
                  << currentRecordingDaemon->name);

    currentRecordingDaemon->st = AsrD_PROCESSING;
    //currentRecordingDaemon->lastTranscription.clear();
    //recordingDaemon.enqueue(currentRecordingDaemon);

    SkArgsMap cmdPck;
    cmdPck["target"] = currentRecordingDaemon->name;
    cmdPck["cmd"] = "START";
    async->publish(daemonCtrlChan, cmdPck);

    async->subscribeChannel(currentRecordingDaemon->transcriptionChan);
    async->attach(publisher->getShortPublisher()->getChannelID(), currentRecordingDaemon->micChan->chanID);

    SkAbstractListIterator<SkDataBuffer *> *itr = inputPcmWindow.getInternalList().iterator();

    while(itr->next())
        inputBuffer.addData(itr->item()->data(), inputBuffer.getCanonicalSize());

    delete itr;

    st = Asr_RECORDING;
    recordingChrono.start();

    ObjectWarning("RECORDING");

    if (conn)
        conn->sendCommand("ASR_CAPTURING", nullptr);
}

void AsrCtrl::stopCurrentRecording()
{
    AssertKiller(!currentRecordingDaemon);

    async->detach(publisher->getShortPublisher()->getChannelID(), currentRecordingDaemon->micChan->chanID);

    SkArgsMap cmdPck;
    cmdPck["target"] = currentRecordingDaemon->name;
    cmdPck["cmd"] = "STOP";

    async->publish(daemonCtrlChan, cmdPck);

    ObjectMessage("Captured pcm-data for " << recordingChrono.stop() << " seconds, from: " << currentRecordingDaemon->name);

    currentRecordingDaemon = nullptr;
    silenceCheckStarted = false;

    ObjectWarning("STOPPED");

    if (conn)
        conn->sendCommand("ASR_BUSY", nullptr);

    if (checkForChangeToBUSY())
    {
        ObjectWarning("There are NOT idle daemons, waiting for first IDLE ..");
        return;
    }

    vuCurrentValue = -1000.f;
    st = Asr_IDLE;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void AsrCtrl::tick()
{
    if (st == Asr_DISABLED)
        return;

    //if (st != Asr_STOPPING)
    {
        if (vuCurrentValue > cfg.silenceThresholdUpper)
        {
            if (silenceCheckStarted)
            {
                ObjectMessage("Continuous-silence-check RESET [" << vuCurrentValue << " > " << cfg.silenceThresholdUpper << " db]");
                silenceCheckStarted = false;
            }

            else if (st == Asr_IDLE)
            {
                ObjectMessage("Voice activity DETECTED  [" << vuCurrentValue << " > " << cfg.silenceThresholdUpper << " db]");
                startCurrentRecording();
            }
        }

        else
        {
            if (st == Asr_RECORDING)
            {
                if (silenceCheckStarted)
                {
                    if (silenceCheckChrono.stop() >= cfg.maxSilenceInterval)
                    {
                        //ObjectMessage("Continuous-silence DETECTED [" << cfg.maxSilenceInterval << " s]; launching STOP procedure  [next " << cfg.bufferingingInterval+0.1f << " s]");
                        silenceCheckStarted = false;
                        /*st = Asr_STOPPING;
                        stoppingChrono.start();
                        ObjectWarning("STOPPING");*/
                        ObjectMessage("Stop capture due to silence max-interval [" << cfg.bufferingingInterval+0.1 << " s]");
                        stopCurrentRecording();
                    }
                }

                else if (vuCurrentValue  < cfg.silenceThresholdLower)
                {
                    silenceCheckStarted = true;
                    silenceCheckChrono.start();
                    ObjectMessage("Continuous-silence check STARTED [" << vuCurrentValue  << " < " << cfg.silenceThresholdLower << " db]; starting silence check procedure  [next " << cfg.maxSilenceInterval << " s]");
                }
            }
        }
    }

    //

    if (st == Asr_RECORDING)
    {
        if (recordingChrono.stop() >= cfg.maxRecordingDuration)
        {
            ObjectMessage("Stop capture due to recording max-interval [" << cfg.maxRecordingDuration << " s]");
            stopCurrentRecording();
        }
    }

    /*else if (st == Asr_STOPPING)
    {
        if (stoppingChrono.stop() >= cfg.bufferingingInterval)
        {
            ObjectMessage("Stop capture due to silence max-interval [" << cfg.bufferingingInterval << " s]");
            stopCurrentRecording();
        }
    }*/
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AsrDaemon *AsrCtrl::selectNextAsrDaemon()
{
    if (st == Asr_DISABLED)
    {
        ObjectWarning("Discarding daemon selection due to DISABLED state");
        return nullptr;
    }

    AsrDaemon *nextDaemon = nullptr;
    SkBinaryTreeVisit<SkString, AsrDaemon *> *itr = daemons.iterator();

    while(itr->next())
    {
        AsrDaemon *daemon = itr->item().value();

        if (daemon->st == AsrD_IDLE)
        {
            nextDaemon = daemon;
            break;
        }
    }

    delete itr;

    if (!nextDaemon)
    {
        ObjectError("CANNOT select NEXT daemon");
        return nullptr;
    }

    nextDaemon->st = AsrD_PROCESSING;
    return nextDaemon;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool AsrCtrl::checkForChangeToIDLE()
{
    if (st != Asr_DISABLED && connectedDaemons == processingDaemons)
        return false;

    ObjectWarning("IDLE");
    st = Asr_IDLE;

    if (conn)
        conn->sendCommand("ASR_IDLE", nullptr);

    return true;
}

bool AsrCtrl::checkForChangeToBUSY()
{
    if (st == Asr_DISABLED || processingDaemons != connectedDaemons)
        return false;

    //IT WILL BECOME IDLE WHEN THE TRANSCRIPTION RETURNs
    ObjectWarning("DISABLED");
    st = Asr_DISABLED;
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool AsrCtrl::isOwnedChannel(SkFlowChanID chanID)
{
    return transcriptionDaemonsChan.contains(chanID);
}

SkArgsMap &AsrCtrl::lastTranscription()
{
    return transcription;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *AsrCtrl::asrStateToString(AsrState st)
{
    if (st == Asr_DISABLED)
        return "DISABLED";

    else if (st == Asr_IDLE)
        return "IDLE";

    else if (st == Asr_RECORDING)
        return "RECORDING";

    else if (st == Asr_STOPPING)
        return "STOPPING";

    return "UNKNOWN_ASR_ST";
}

CStr *AsrCtrl::asrDaemonStateToString(AsrDaemonState st)
{
    if (st == AsrD_OFFLINE)
        return "DISABLED";

    else if (st == AsrD_IDLE)
        return "IDLE";

    else if (st == AsrD_PROCESSING)
        return "PROCESSING";

    else if (st == AsrD_PROCESSED)
        return "PROCESSED";

    return "UNKNOWN_ASR_D_ST";
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

