#ifndef LANGCTRL_H
#define LANGCTRL_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Network/FlowNetwork/skflowgenericsubscriber.h>

#include "abstractcontroller.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct LangConfig
{
    SkString nlpName;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class LangCtrl extends AbstractController
{
    LangConfig cfg;

    SkString promptServiceName;
    SkFlowChanID promptServiceID;

    SkString generatedChanName;

    SkFlowGenericSubscriber *nlpGeneration;
    SkQueue<SkString> tokensQueue;
    SkString generatedBlock;

    bool processing;

    public:
        Constructor(LangCtrl, AbstractController);

        void setup(LangConfig &config);

        void onChannelAdded(SkFlowChannel *ch);
        void onChannelRemoved(SkFlowChannel *ch);

        void execPrompt(CStr *prompt);

        void onFlowDataCome(SkFlowChannelData &dataContainer)   override;
        bool isOwnedChannel(SkFlowChanID chanID)                override;

        Slot(onNlpReady);
        Slot(onNlpDataCome);
        Signal(onNlpBlockReady);

        Slot(onServiceResponseReceived);

        bool isReady();
        bool isProcessing();
        bool consume(SkString &txt);

    private:
        void onInit()                                           override;
        void onStart()                                          override;
        void onStop()                                           override;
        void onClose()                                          override;
        void onSetConnection()                                  override;
        void onUnSetConnection()                                override;
        void onFastTick()                                       override;
        void onSlowTick()                                       override;
        void onOneSecondTick()                                  override;

        void evaluateNlpData();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // LANGCTRL_H
